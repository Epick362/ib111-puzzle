import random
from Card import *

class Board:
    def __init__(self, size):
        self.cols = size[0]
        self.rows = size[1]

        self.firstCard = None
        self.secondCard = None

        self.cardsCount = self.rows * self.cols

        if self.cardsCount % 2 == 0:
            self.cardTypesCount = self.cardsCount / 2
            # print 'initialize board with size %s x %s' % (self.cols, self.rows)
        else:
            raise ValueError('There must be an even number of cards on the board')

    def generate(self):
        self.cards = []
        self.cardTypes = [{'type': i, 'count': 0} for i in xrange(self.cardTypesCount)]

        for i in xrange(self.rows):
            row = []
            for j in xrange(self.cols):
                index = random.randint(0, len(self.cardTypes) - 1)
                card = Card(self.cardTypes[index]['type'])

                # print 'Card type %s generated at %s, %s' % (card.type, i, j)

                row.append(card)

                self.cardTypes[index]['count'] += 1
                if self.cardTypes[index]['count'] == 2:
                    self.cardTypes.pop(index)

            self.cards.append(row)

        # print self.cards

    def turnOverCard(self, cardPos):
        card = self.cards[cardPos[1]][cardPos[0]]

        if self.firstCard is None:
            self.firstCard = card
            self.firstCardPos = (cardPos[1], cardPos[0])
        else:
            self.secondCard = card
            self.secondCardPos = (cardPos[1], cardPos[0])
            self.checkForMatch()

        return card

    def checkForMatch(self):
        if self.firstCardPos != self.secondCardPos:
            if self.firstCard != 0 and self.secondCard != 0 and self.firstCard.type == self.secondCard.type:
                self.cards[self.firstCardPos[0]][self.firstCardPos[1]] = 0
                self.cards[self.secondCardPos[0]][self.secondCardPos[1]] = 0

                #print '%s = %s match found at [%s, %s] and [%s, %s]' % (self.firstCard.type, self.secondCard.type, self.firstCardPos[0], self.firstCardPos[1], self.secondCardPos[0], self.secondCardPos[1])

        self.firstCard = None
        self.secondCard = None

    def checkGameEnd(self):
        gameEnded = True

        for row in self.cards:
            for card in row:
                if isinstance(card, Card):
                    gameEnded = False

        return gameEnded
