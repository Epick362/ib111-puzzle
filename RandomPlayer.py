import random
from Player import *

class RandomPlayer(Player):
    def __init__(self, gameSize):
        self.gameSize = gameSize
        # print 'Player random initialized'

    def determineFirstCard(self):
        return (random.randint(0, self.gameSize[0] - 1), random.randint(0, self.gameSize[1] - 1))

    def determineSecondCard(self):
        return (random.randint(0, self.gameSize[0] - 1), random.randint(0, self.gameSize[1] - 1))
