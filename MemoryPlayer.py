import random
from Player import *
from Card import *

class MemoryPlayer(Player):
    def __init__(self, gameSize, memorySize):
        self.gameSize = gameSize
        self.cardMemory = {}
        self.firstCard = None
        self.memorySize = memorySize
        # print 'Player type memory initialized'

    def determineFirstCard(self):
        randPos = self.randomPosNotInMemory()
        return randPos

    def determineSecondCard(self):
        fromMemory = self.getCardPosFromMemory()
        if fromMemory is not False:
            return fromMemory

        randPos = self.randomPosNotInMemory()
        return randPos

    def turnedOverCard(self, card, position):
        if isinstance(card, Card):
            if self.firstCard is None:
                # first
                self.firstCard = card
                self.firstCardPos = position
            else:
                # second
                self.storeIntoMemory(self.firstCard.type, self.firstCardPos)
                self.storeIntoMemory(card.type, position)

                self.firstCard = None

    def randomPosNotInMemory(self):
        while True:
            inMemory = False
            randPos = (random.randint(0, self.gameSize[0] - 1), random.randint(0, self.gameSize[1] - 1))
            for key in self.cardMemory:
                if randPos == self.cardMemory[key]:
                    inMemory = True

            if not inMemory:
                break

        return randPos

    def getCardPosFromMemory(self):
        if isinstance(self.firstCard, Card) and self.firstCard.type in self.cardMemory:
            return self.cardMemory[self.firstCard.type]

        return False

    def storeIntoMemory(self, type, pos):
        if type not in self.cardMemory:
            if isinstance(self.memorySize, int) and len(self.cardMemory) > self.memorySize:
                self.cardMemory.pop(random.choice(self.cardMemory.keys()))

            self.cardMemory.update({type: pos})

    def reset(self):
        self.cardMemory = {}
        self.firstCard = None
