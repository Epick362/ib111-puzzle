from Board import *
import time

class Game():
    def __init__(self, size, player):
        self.board = Board(size)
        self.board.generate()

        self.ended = False
        self.turns = 0

        self.player = player

    def turn(self):
        firstCardPos = self.player.determineFirstCard()
        firstCard = self.board.turnOverCard(firstCardPos)
        self.player.turnedOverCard(firstCard, firstCardPos)

        secondCardPos = self.player.determineSecondCard()
        secondCard = self.board.turnOverCard(secondCardPos)
        self.player.turnedOverCard(secondCard, secondCardPos)

        self.turns += 1

        if self.board.checkGameEnd():
            self.ended = True
