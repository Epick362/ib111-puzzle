# main game setup here
from Game import *
from RandomPlayer import *
from MemoryPlayer import *
from Statistics import *
import matplotlib.pyplot as plt
import numpy as np

gameSize = (10, 10)

memorySize = (10, 20, 30, 40)
players = (MemoryPlayer(gameSize, memorySize[0]), MemoryPlayer(gameSize, memorySize[1]), MemoryPlayer(gameSize, memorySize[2]), MemoryPlayer(gameSize, memorySize[3]))

playerTurns = []
numberOfGames = 20
meanNumberOfTurns = []

for player in players:
    totalTurns = 0
    gamesTurns = []
    for i in xrange(numberOfGames):
        _game = Game(gameSize, player)
        while not _game.ended:
            _game.turn()

        player.reset()
        gamesTurns.append(_game.turns)

    playerTurns.append(gamesTurns)

    for turn in gamesTurns:
        totalTurns = totalTurns + turn

    print 'Mean number of turns %s' % (totalTurns / numberOfGames)

    meanNumberOfTurns.append(totalTurns / numberOfGames);

plt.plot(memorySize, meanNumberOfTurns)
plt.title('Pexeso simulator - Memory size / game length')
plt.ylabel('Mean n. of Turns')
plt.xlabel('Memory Size')
plt.show()

plt.bar(np.arange(20), playerTurns[0], color='crimson', label='MemoryPlayer(10)', alpha=0.8)
plt.bar(np.arange(20), playerTurns[1], color='blue', label='MemoryPlayer(10)', alpha=0.8)
plt.bar(np.arange(20), playerTurns[2], color='green', label='MemoryPlayer(10)', alpha=0.8)
plt.bar(np.arange(20), playerTurns[3], color='yellow', label='MemoryPlayer(10)', alpha=0.8)

plt.title('Pexeso simulator')
plt.ylabel('Turns')
plt.xlabel('Game Number')
plt.show()
